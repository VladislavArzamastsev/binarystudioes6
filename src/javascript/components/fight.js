import { controls } from '../../constants/controls';

const keysDownSet = new Set();

class FighterInfo {

  constructor(initialHealth, attackCode, blockCode) {
    this.initialHealth = initialHealth;
    this.currentHealth = initialHealth;
    this.criticalCodes = new Set();
    this.attackCode = attackCode;
    this.blockCode = blockCode;
  }

  hasCriticalCode(code) {
    return this.criticalCodes.has(code);
  }

  setCriticalCodes(codes) {
    this.criticalCodes = codes;
  }

  getAttackCode() {
    return this.attackCode;
  }

  getBlockCode() {
    return this.blockCode;
  }

  decrementCurrentHealth(amount) {
    this.currentHealth = Math.max(this.currentHealth - amount, 0);
  }

  getInitialHealth() {
    return this.initialHealth;
  }

  getCurrentHealth() {
    return this.currentHealth;
  }

}

let leftPlayerInfo;
let rightPayerInfo;

export async function fight(firstFighter, secondFighter) {
  leftPlayerInfo = new FighterInfo(firstFighter.health,
    controls.PlayerOneAttack, controls.PlayerOneBlock);
  leftPlayerInfo.setCriticalCodes([
    controls.PlayerOneCriticalHitCombination[0],
    controls.PlayerOneCriticalHitCombination[1],
    controls.PlayerOneCriticalHitCombination[2]]);
  rightPayerInfo = new FighterInfo(secondFighter.health,
    controls.PlayerTwoAttack, controls.PlayerTwoBlock);
  rightPayerInfo.setCriticalCodes([
    controls.PlayerTwoCriticalHitCombination[0],
    controls.PlayerTwoCriticalHitCombination[1],
    controls.PlayerTwoCriticalHitCombination[2]]);

  window.document.onkeydown = function(e) {
    keysDownSet.add(e.code);
    handleKeyDown(e, firstFighter, secondFighter);
    changeHealthBarWidth(leftPlayerInfo, 'left');
    changeHealthBarWidth(rightPayerInfo, 'right');
  };
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    if (leftPlayerInfo.getCurrentHealth() === 0) {
      resolve(secondFighter);
    } else if (rightPayerInfo.getCurrentHealth() === 0) {
      resolve(firstFighter);
    }
  });
}

function handleKeyDown(event, firstFighter, secondFighter) {
  switch (event.code) {
    case leftPlayerInfo.getAttackCode():
      if (canPerformHit()) {
        rightPayerInfo.decrementCurrentHealth(getDamage(firstFighter, secondFighter));
      }
      keysDownSet.delete(leftPlayerInfo.getAttackCode());
      break;
    case controls.PlayerOneCriticalHitCombination[0]:
    case controls.PlayerOneCriticalHitCombination[1]:
    case controls.PlayerOneCriticalHitCombination[2]:
      rightPayerInfo.decrementCurrentHealth(getCriticalDamage(firstFighter));
      removeCodeIfContains(leftPlayerInfo);
      break;
    case rightPayerInfo.getAttackCode():
      if (canPerformHit()) {
        leftPlayerInfo.decrementCurrentHealth(getDamage(secondFighter, firstFighter));
      }
      keysDownSet.delete(rightPayerInfo.getAttackCode());
      break;
    case controls.PlayerTwoCriticalHitCombination[0]:
    case controls.PlayerTwoCriticalHitCombination[1]:
    case controls.PlayerTwoCriticalHitCombination[2]:
      leftPlayerInfo.decrementCurrentHealth(getCriticalDamage(secondFighter));
      removeCodeIfContains(rightPayerInfo);
      break;
  }
}

function removeCodeIfContains(playerInfo) {
  keysDownSet.forEach(function(v) {
    if (playerInfo.hasCriticalCode(v)) {
      keysDownSet.delete(v);
    }
  });
}

function changeHealthBarWidth(playerInfo, position) {
  const bar = document.getElementById(position + '-fighter-indicator');
  if (bar.style.width === '') {
    bar.style.width = '100%';
  }
  const newWidth = playerInfo.getCurrentHealth() / playerInfo.getInitialHealth() * 100;
  bar.style.width = String(newWidth) + '%';
}

function canPerformHit() {
  return !(keysDownSet.has(leftPlayerInfo.getBlockCode()) || keysDownSet.has(rightPayerInfo.getBlockCode()));
}

function getCriticalDamage(attacker) {
  return 2 * attacker.attack;
}

export function getDamage(attacker, defender) {
  const powerDelta = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(powerDelta, 0);
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

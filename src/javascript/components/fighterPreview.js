import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  fighterElement.append(createFighterImageHolder(fighter, position));
  fighterElement.append(createPreviewDiv("Name: " + fighter.name));
  fighterElement.append(createPreviewDiv("Health: " + fighter.health));
  fighterElement.append(createPreviewDiv("Attack: " + fighter.attack));
  fighterElement.append(createPreviewDiv("Defense: " + fighter.defense));

  return fighterElement;
}

function createFighterImageHolder(fighter, position, ){
  const imgElement = createFighterImage(fighter);
  const imgPositionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterHolder = createElement({
    tagName: 'div',
    className: `arena___fighter ${imgPositionClassName}`
  });
  fighterHolder.append(imgElement);
  return fighterHolder;
}

function createPreviewDiv(content){
  const paragraph = createElement({
    tagName: "div",
    className: "arena___fighter-name"
  });
  const contentHolder = document.createTextNode(content);

  paragraph.appendChild(contentHolder);
  return paragraph;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

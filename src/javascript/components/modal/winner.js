import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = 'Winner: ' + fighter.name;
  showModal({ title, fighter });

}
